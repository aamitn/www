<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head><div class="white-area-content">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> Test Full Page</div>
    <div class="db-header-extra"> 
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>">Home</a></li>
  <li class="active">APP PANEL</li>
</ol>

<p>Run our application's demo versins in your browser!</p>

<hr>
<h3>Use These Buttons to open app in new window</h3>
<p>
<a href="rg/robosim.html" target="_blank"><input type="button" class="btn btn-primary" value="Use KineSim™" /> </a>
<a href="ckt/circuitjs.html" target="_blank"><input type="button" class="btn btn-primary" value="Use SimC™" /> </a>
<a href="http://nandimechatronics.me"><input type= "button" class="btn btn-success" value="NMPL HOME" /> </a>
<a href="http://nandimechatronics.me"><input type= "button" class="btn btn-danger" value="LOGOUT" /> </a>
<hr>

<div class="w3-container">
  <h3>Or Run it here by clicking the tabs below!</h3>


  <div class="w3-bar w3-black">
    <button class="w3-bar-item w3-button tablink w3-red" onclick="openCity(event,'London')"></button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Paris')">KineSim™</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Tokyo')">SimC™</button>
  </div>
  
  <div id="London" class="w3-container w3-border city">


  </div>

  <div id="Paris" class="w3-container w3-border city" style="display:none">
    <h2>Loading KineSim™... </h2>
  <iframe style="height:600px;width:1100px;" src="rg/robosim.html"></iframe>
  </div>

  <div id="Tokyo" class="w3-container w3-border city" style="display:none">
    <h2>Loading SimC™... </h2>
  <iframe style="height:800px;width:1100px;" src="ckt/circuitjs.html"></iframe>
  </div>
</div>

<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>

</div>